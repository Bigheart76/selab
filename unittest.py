import unittest

from e import specify_gender
class Unittest(unittest.TestCase):
	def test_ahmad(self):
		self.assertEqual(specify_gender("ahmad"), "boy")
	
	def test_motahare(self):
		self.assertEqual(specify_gender("motahare"), "girl")